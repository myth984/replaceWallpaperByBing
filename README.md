# replaceWallpaperByBing

**将bing图片设为windows壁纸**

方法：

1. 下载bing首页图片至程序运行目录

2. 通过cmd命令修改注册表修改壁纸

3. 下次开机，壁纸修改成功


**注意事项：**

1. 请保持网络通畅（没有处理网络异常）
2. 如需开机启动：

	(1) 将程序打包成可执行jar包或 用我已经打包好的replaceWallpaperByBing.jar
	
	(2) 放入windows启动目录                         
	参考:[Windows 10当中如何添加开机启动应用](https://answers.microsoft.com/zh-hans/windows/forum/windows_10-other_settings/windows/a81e83eb-a079-4b3c-9d8f-facc9ed03871?tm=1442997086548)

**待解决的问题：**

1. 修改注册表后立即刷新壁纸，即达到开机就可修改壁纸的目的，而不是要等到下次开机。（重启explorer.exe无效，希望大佬提出解决办法）


**replace windows wallpaper from Bing's background**

**Mehod：**
1. Download the picture of Bing homepage to the program running directory

2. Modify the wallpaper by modifying the registry with the CMD command

3. Next time the machine starts, the wallpaper is modified successfully.


**Matters needing attention：**
1. Please keep the network unobstructed (no network anomalies are handled)
2. Start up if necessary:
	(1) Packing programs into executable jar packages or using packages I've already packaged
	(2) Put it in the Windows Startup Directory  
    [for example](https://answers.microsoft.com/zh-hans/windows/forum/windows_10-other_settings/windows/a81e83eb-a079-4b3c-9d8f-facc9ed03871?tm=1442997086548)
		


**Problems to be solved：**
1. Refresh the wallpaper immediately after modifying the registry, you can modify the wallpaper when you start the machine, instead of waiting until the next boot. (Restart explorer.exe is not valid, I hope you will come up with a solution)


