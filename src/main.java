

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class main {



     public static void main(String[] args) throws Exception {
         //设置bing地址

         final String encoding="UTF-8";
         final String u="https://cn.bing.com";
         final String pre="href=\"";
         final String post="as=\"image\"";
         // 根据链接（字符串格式），生成一个URL对象
         URL url = new URL(u);
         // 打开URL
         HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
         // 得到输入流
         BufferedReader reader = new BufferedReader(new InputStreamReader(
                 urlConnection.getInputStream(), encoding));
         // 读取输入流的数据，并显示
         String line;
         while ((line = reader.readLine()) != null) {

             if(line.indexOf("id=\"bgLink\"")!=-1)

                 break;
         }
         //获得图片位置
         int pre_index=line.indexOf(pre)+pre.length();
         int post_index=line.indexOf(post)-1;
         //获得img地址
         String imgurl=u+
                 line.substring(pre_index,post_index);
         System.out.println(imgurl);

         String imgname=imgurl.substring(
                 imgurl.indexOf("id=")+3,imgurl.indexOf("ZH")-1
         );
         url = new URL(imgurl);
         //打开url
         urlConnection = (HttpURLConnection) url.openConnection();
         //存入byte数组
         byte[] data = readInputStream(urlConnection.getInputStream());
         //根据文件名创建文件
         File imageFile = new File(imgname+".bmp");
         //创建文件输出流
         FileOutputStream outStream = new FileOutputStream(imageFile);
         //写出
         outStream.write(data);
         //关闭连接
         outStream.close();
         String imgAbsPath=imageFile.getAbsolutePath();
         //执行cmd命令
         Runtime runtime=Runtime.getRuntime();
         //修改注册表
         runtime.exec("cmd.exe  /k msg  %username% /time:10  下次开机更换壁纸 &&" +
                 "reg add \"hkcu\\control panel\\desktop\" /v wallpaper /d \""+imgAbsPath+"\" /f &&" +
                 "RunDll32.exe USER32.DLL,UpdatePerUserSystemParameters &&" +
                 "exit "
                  );

         //https://blog.csdn.net/weixin_38310965/article/details/80392767
         //https://answers.microsoft.com/zh-hans/windows/forum/windows_10-other_settings/windows/a81e83eb-a079-4b3c-9d8f-facc9ed03871?tm=1442997086548

     }


    public static byte[] readInputStream(InputStream inStream) throws Exception{
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        //创建一个Buffer字符串
        byte[] buffer = new byte[1024];
        //每次读取的字符串长度，如果为-1，代表全部读取完毕
        int len = 0;
        //使用一个输入流从buffer里把数据读取出来
        //instream.read返回长度，将内容写入buffer
        while( (len=inStream.read(buffer)) != -1 ){
            //用输出流往buffer里写入数据，中间参数代表从哪个位置开始读，len代表读取的长度
            outStream.write(buffer, 0, len);
        }
        //关闭输入流
        inStream.close();
        //把outStream里的数据写入内存
        return outStream.toByteArray();
    }
}
